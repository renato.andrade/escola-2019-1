package br.ucsal.bes20191.testequalidade.escola.tui;

public class TuiHelperUnitarioTest {

	/**
	 * Verificar a obtenção do nome completo. Caso de teste: primeiro nome
	 * "Claudio" e sobrenome "Neiva" resulta no nome "Claudio Neiva".
	 */
	public void testarObterNomeCompleto() {
	}

	/**
	 * Verificar a obtenção exibição de mensagem. Caso de teste: mensagem "Tem que estudar." resulta em "Bom dia! Tem que estudar.".
	 */
	public void testarExibirMensagem() {
	}

}
