package br.ucsal.bes20191.testequalidade.escola.domain;

public enum SituacaoAluno {
	ATIVO, CANCELADO;
}
